﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacSummerService.Services
{
    public class SummerService : GetSummerZodiacService.GetSummerZodiacServiceBase
    {
        private const int MAX_DAY_MONTH = 31;
        private const int MIN_DAY_MONTH = 1;
        private readonly string zodiacFile = "../../ZodiacService/ZodiacSummerService/Resources/SummerZodiac.txt";

        private readonly ILogger<SummerService> _logger;
        public SummerService(ILogger<SummerService> logger)
        {
            _logger = logger;
        }

        public ZodiacName StringToZodiac(string zodiacString)
        {
            switch (zodiacString)
            {
                case "Fecioara":
                    return ZodiacName.Fecioara;
                case "Gemeni":
                    return ZodiacName.Gemeni;
                case "Leu":
                    return ZodiacName.Leu;
                case "Rac":
                    return ZodiacName.Rac;
            }
            return ZodiacName.Invalid;
        }

        private ZodiacName GetSummerZodiacName(int day, int month, int year)
        {

            var zodiacList = File.ReadAllLines(zodiacFile).ToList();
            List<string> springZodiacs = new List<string>();
            foreach (var interval in zodiacList)
            {
                springZodiacs.Add(interval.Replace(" ", String.Empty));
            }

            foreach (var springZodiac in springZodiacs)
            {
                var zodiacInterval = springZodiac.Split("-").ToList();
                var firstInterval = zodiacInterval.ElementAt(0).Split("/").ToList();
                var secondInterval = zodiacInterval.ElementAt(1).Split("/").ToList();
                var firstIntervalMonth = firstInterval.ElementAt(1);
                var secondIntervalMonth = secondInterval.ElementAt(1);

                if (month == Convert.ToInt32(firstIntervalMonth))
                {
                    var intervalStartDay = Convert.ToInt32(firstInterval.ElementAt(0));
                    var intervalEndDay = firstIntervalMonth == secondIntervalMonth ? Convert.ToInt32(secondInterval.ElementAt(0)) : MAX_DAY_MONTH;
                    if (intervalStartDay <= day && intervalEndDay >= day)
                    {
                        var zodiac = zodiacInterval.ElementAt(2);
                        return StringToZodiac(zodiac);
                    }
                }
                if (month == Convert.ToInt32(secondIntervalMonth))
                {
                    var intervalStartDay = MIN_DAY_MONTH;
                    var intervalEndDay = Convert.ToInt32(secondInterval.ElementAt(0));
                    if (intervalStartDay <= day && intervalEndDay >= day)
                    {
                        var zodiac = zodiacInterval.ElementAt(2);
                        return StringToZodiac(zodiac);
                    }
                }
            }

            return ZodiacName.Invalid;
        }

        public override Task<ZodiacDataResponse> GetZodiacDataRequest(Date request, ServerCallContext context)
        {
            return Task.FromResult(new ZodiacDataResponse
            {
                ZodiacName = GetSummerZodiacName(request.Day, request.Month, request.Year)
            });
        }
    }
}
