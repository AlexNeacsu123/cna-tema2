﻿using Grpc.Core;
using Grpc.Net.Client;
using System;

namespace ZodiacClient
{
    class Program
    {
        private static GrpcChannel channel;
        private static GetZodiacService.GetZodiacServiceClient client;

        static void Main(string[] args)
        {
            channel = GrpcChannel.ForAddress("https://localhost:5001");
            client = new GetZodiacService.GetZodiacServiceClient(channel);

            char option = ' ';
            do
            {
                try
                {
                    Console.WriteLine("Y - Send data to the server");
                    Console.WriteLine("N - Exit the app");
                    option = Console.ReadKey().KeyChar;
                    Console.Clear();
                    switch (option)
                    {
                        case 'Y':
                            {
                                Console.WriteLine("\nInsert birthdate:");
                                var date = Console.ReadLine();
                                if(String.IsNullOrWhiteSpace(date))
                                {
                                    Console.WriteLine("\nInvalid birthdate\n");
                                    break;
                                }

                                var result = client.GetZodiacDataRequest(new Date
                                {
                                    CalendaristicDate = date
                                });

                                if (result.ZodiacName.ToString() != String.Empty)
                                {
                                    Console.WriteLine("\nClient has the zodiac " + result.ZodiacName.ToString() + "\n");
                                }
                                break;
                            }

                        case 'N':
                            {
                                Console.WriteLine("\nExiting app\n");
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("\nInvalid option\n");
                                break;
                            }
                    }
                }
                catch (RpcException e)
                {
                    Console.WriteLine(e.Status.Detail);
                }
            } while (option != 'N');

            channel.ShutdownAsync();
        }
    }
}
